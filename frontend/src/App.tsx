import { Redirect, Route, Switch } from 'react-router-dom';
import { IonApp, IonRouterOutlet } from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';

import FirstWindow from './components/FirstWindow';
import Cookies from 'universal-cookie';
import React from 'react';
import './App.css';
import BoardPage from './pages/BoardPage/BoardPage';
import BusRoutePage from './pages/BusRoutePage/BusRoutePage';
import AboutUs from './components/MenuItems/AboutUs';

const routes = [
  {
    path: '/board',
    component: BoardPage,
  },
  {
    path: '/busRoutePage/:id',
    component: BusRoutePage,
  },
  {
    path: '/AboutUs',
    component: AboutUs,
  },
];

const App: React.FC = () => {
  const cookies = new Cookies();

  return (
    <IonApp class="pageApp">
      {!cookies.get('isNotFirstVisit') && <FirstWindow />}
      <IonReactRouter>
        <IonRouterOutlet>
          <Switch>
            {routes.map((x) => (
              <Route key={x.path} {...x} exact />
            ))}
            <Route exact path="/">
              <Redirect to={routes[0].path} />
            </Route>
          </Switch>
        </IonRouterOutlet>
      </IonReactRouter>
    </IonApp>
  );
};

export default App;
