import {
  IonApp,
  IonHeader,
  IonToolbar,
  IonTitle,
  IonContent,
  IonList,
  IonMenuButton,
  IonCol,
  IonRow,
  IonGrid,
  IonButton,
} from '@ionic/react';
import React from 'react';
import { useHistory } from 'react-router-dom';
import Menu from '../../components/Menu/Menu';
import { useBusProvider } from '../../context/busDataContext';
import _ from 'lodash';
import styles from './BoardPage.module.scss';

const BoardPage: React.FC = () => {
  const history = useHistory();
  const busProvider = useBusProvider();

  return (
    <IonApp>
      <IonHeader>
        <IonToolbar>
          <IonMenuButton className={styles.menuBtnClass} slot="start" id="mainContent" />
          <IonTitle>Board</IonTitle>
        </IonToolbar>
      </IonHeader>
      <Menu />
      <IonContent className={styles.contBoard}>
        <IonList>
          <IonGrid className={styles.gridClass}>
            <IonRow>
              <IonCol>
                {_.uniqBy(busProvider.buses, (x) => x.numberRoute).map((x) => (
                  <IonButton
                    key={x.numberRoute}
                    color="light"
                    className={styles.busBtnClass}
                    onClick={() => history.push('/busRoutePage/' + x.numberRoute)}>
                    {x.numberRoute}
                  </IonButton>
                ))}
              </IonCol>
            </IonRow>
          </IonGrid>
        </IonList>
      </IonContent>
    </IonApp>
  );
};

export default BoardPage;
