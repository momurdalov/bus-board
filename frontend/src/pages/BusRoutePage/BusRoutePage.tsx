import React, { useState } from 'react';
import {
  IonBackButton,
  IonButtons,
  IonHeader,
  IonPage,
  IonToolbar,
  IonTitle,
  IonContent,
  IonButton,
  IonIcon,
  IonCard,
  IonCol,
  IonGrid,
  IonRow,
  IonText,
  IonAlert,
  IonCheckbox,
  IonItem,
  IonLabel,
} from '@ionic/react';
import { useHistory, useParams } from 'react-router-dom';
import { arrowBackOutline } from 'ionicons/icons';
import { useBusProvider } from '../../context/busDataContext';
import MyMap from '../../components/Map/MyMap';
import styles from './BusRoutePage.module.scss';

const BusRoutePage: React.FC = (props) => {
  const history = useHistory();

  const [checked, setChecked] = useState(true);

  const { id } = useParams<{ id: string }>();
  const busProvider = useBusProvider();
  const bus = busProvider.buses.find((x) => x.numberRoute == id);

  const [activeStopping, setActiveStopping] = useState<number[]>();

  const [showAlert1, setShowAlert1] = useState(false);

  if (bus === undefined) {
    return (
      <IonAlert
        isOpen={showAlert1}
        onDidDismiss={() => setShowAlert1(false)}
        header={'Неверные данные'}
        buttons={['OK']}
      />
    );
  }

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonButton
              onClick={() => {
                history.goBack();
              }}>
              <IonIcon icon={arrowBackOutline} />
            </IonButton>
          </IonButtons>
          <IonTitle>Маршрут {bus?.numberRoute}</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonGrid>
          <IonRow>
            <IonCol>
              <IonCard>
                <MyMap markerVisible={checked} activeStopping={activeStopping} bus={bus} />
              </IonCard>
            </IonCol>
          </IonRow>
          <IonRow>
            <IonCol>
              <IonItem className={styles.checkClass}>
                <IonText>Скрыть метки</IonText>
                <IonCheckbox
                  color="dark"
                  slot="start"
                  className={styles.chekboxClass}
                  onIonChange={() => {
                    if (checked) {
                      setChecked(false);
                    } else {
                      setChecked(true);
                    }
                  }}
                />
              </IonItem>
            </IonCol>
          </IonRow>
          <IonRow>
            <IonCol>
              <IonCard className={styles.priceClass}>
                <IonText>Цена за проезд: {bus.price}</IonText>
              </IonCard>
            </IonCol>
            <IonCol>
              <IonCard className={styles.fromToClass}>
                <IonText>График: {bus?.fromTo}</IonText>
              </IonCard>
            </IonCol>
          </IonRow>
          <IonRow>
            <IonCol>
              <IonCard className={styles.stopaClass}>
                <IonRow>
                  <IonCol>
                    <IonText>↑ Список остановок: ↓ </IonText>
                  </IonCol>
                </IonRow>
                <IonRow>
                  <IonCol>
                    {bus?.pathTo
                      .filter((x) => x.name !== undefined)
                      .map((x) => (
                        <IonCard
                          className={styles.stopClass}
                          onClick={() => setActiveStopping(x.coordinates)}>
                          {x.name}
                        </IonCard>
                      ))}
                  </IonCol>
                  <IonCol>
                    {bus?.pathFrom
                      .filter((x) => x.name !== undefined)
                      .map((x) => (
                        <IonCard
                          className={styles.stopClass}
                          onClick={() => setActiveStopping(x.coordinates)}>
                          {x.name}
                        </IonCard>
                      ))}
                  </IonCol>
                </IonRow>
              </IonCard>
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>
    </IonPage>
  );
};

export default BusRoutePage;
