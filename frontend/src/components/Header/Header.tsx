import './BoardPage.css';

import React from 'react';
import { useHistory } from 'react-router-dom';
import { IonHeader, IonMenuButton, IonTitle, IonToolbar } from '@ionic/react';

const Header: React.FC = () => {
  const history = useHistory();
  return (
    <IonHeader>
      <IonToolbar>
        <IonMenuButton slot="start" id="mainContent" />
        <IonTitle>Board</IonTitle>
      </IonToolbar>
    </IonHeader>
  );
};

export default Header;
