import {
  IonPage,
  IonHeader,
  IonToolbar,
  IonButtons,
  IonButton,
  IonIcon,
  IonTitle,
  IonContent,
  IonText,
  IonCard,
} from "@ionic/react";
import { arrowBackOutline } from "ionicons/icons";
import React from "react";
import { useHistory } from "react-router-dom";
import styles from "./AboutUs.module.scss";

const AboutUs: React.FC = (props) => {
  const history = useHistory();

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonButton
              onClick={() => {
                history.goBack();
              }}
            >
              <IonIcon icon={arrowBackOutline} />
            </IonButton>
          </IonButtons>
          <IonTitle>О нас</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonCard className={styles.cardTextClass}>
          <IonText>
            Здравствуйте!
            <br /> Данное приложение является дипломным поектом студента
            Чеченского Государственного Университета факультета информационных
            технологий Мурдалова Магомеда.
          </IonText>
        </IonCard>
      </IonContent>
    </IonPage>
  );
};

export default AboutUs;
