import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import {
  IonContent,
  IonHeader,
  IonIcon,
  IonItem,
  IonLabel,
  IonList,
  IonMenu,
  IonMenuButton,
  IonModal,
  IonTitle,
  IonToggle,
  IonToolbar,
} from '@ionic/react';
import { moon } from 'ionicons/icons';
import styles from './Menu.module.scss';

const Menu: React.FC = () => {
  // const toggleDarkModeHandler = () => {
  //   document.body.classList.toggle("dark");
  // };

  // Use matchMedia to check the user preference
  const prefersDark = window.matchMedia('(prefers-color-scheme: dark)');

  toggleDarkTheme(prefersDark.matches);

  // Listen for changes to the prefers-color-scheme media query
  prefersDark.addListener((mediaQuery) => toggleDarkTheme(mediaQuery.matches));

  // Add or remove the "dark" class based on if the media query matches
  function toggleDarkTheme(shouldAdd: boolean | undefined) {
    document.body.classList.toggle('dark', shouldAdd);
  }

  const history = useHistory();

  return (
    <IonMenu side="start" contentId="mainContent">
      <IonHeader>
        <IonToolbar className={styles.toolbarClass}>
          <IonTitle>Bus Board</IonTitle>
        </IonToolbar>
      </IonHeader>

      <IonContent>
        <IonList className={styles.listClass}>
          <IonItem className={styles.itemClass} onClick={() => history.push('/AboutUs')}>
            О нас
          </IonItem>
          {/* <IonItem>
            <IonIcon slot="start" icon={moon} className="component-icon component-icon-dark" />
            <IonLabel>Dark Mode</IonLabel>
            <IonToggle slot="end" name="darkMode" onIonChange={toggleDarkModeHandler} />
          </IonItem> */}
        </IonList>
      </IonContent>
    </IonMenu>
  );
};

export default Menu;
