import React, { useCallback, useState } from 'react';
import { YMaps, Map, Placemark, Polyline, GeolocationControl, GeoObject } from 'react-yandex-maps';
import { IBus, useBusProvider } from '../../context/busDataContext';
import styles from './MyMap.module.scss';

interface IMyMap {
  bus: IBus;
  activeStopping?: number[];
  setActiveStopping?: (coords: number[]) => void;
  markerVisible?: boolean;
}

const MyMap: React.FC<IMyMap> = (props) => {
  const defaultCenter = props.bus?.pathTo[0].coordinates;

  return (
    <div>
      <YMaps>
        <div>
          <Map
            state={{ center: props.activeStopping ?? defaultCenter, zoom: 12 }}
            defaultState={{ center: props.activeStopping ?? defaultCenter, zoom: 12 }}
            width="100%"
            height="300px">
            {props.bus?.pathTo
              .filter((x) => x.name !== undefined)
              .map((x) => (
                <Placemark
                  key={x.coordinates.toString()}
                  name="marker"
                  onClick={() => {
                    if (props.setActiveStopping) props.setActiveStopping(x.coordinates);
                  }}
                  properties={{ iconCaption: props.activeStopping == x.coordinates ? x.name : '' }}
                  geometry={x.coordinates}
                  options={{
                    visible: props.markerVisible,
                    preset: 'islands#blueCircleDotIcon',
                  }}
                />
              ))}
            {props.bus?.pathFrom
              .filter((x) => x.name !== undefined)
              .map((x) => (
                <Placemark
                  key={x.coordinates.toString()}
                  name="marker"
                  onClick={() => {
                    if (props.setActiveStopping) props.setActiveStopping(x.coordinates);
                  }}
                  properties={{ iconCaption: props.activeStopping == x.coordinates ? x.name : '' }}
                  geometry={x.coordinates}
                  options={{
                    visible: props.markerVisible,
                    preset: 'islands#blueCircleDotIcon',
                  }}
                />
              ))}
            {props.bus?.pathTo
              .filter((x) => x.name !== undefined)
              .map((x) => (
                <Polyline
                  geometry={props.bus?.pathTo.map((x) => x.coordinates)}
                  options={{
                    balloonCloseButton: false,
                    strokeColor: '#ff0000',
                    strokeWidth: 4,
                    strokeOpacity: 1,
                  }}
                />
              ))}
            {props.bus?.pathFrom
              .filter((x) => x.name !== undefined)
              .map((x) => (
                <Polyline
                  geometry={props.bus?.pathFrom.map((x) => x.coordinates)}
                  options={{
                    balloonCloseButton: false,
                    strokeColor: '#00ff00',
                    strokeWidth: 3,
                    strokeOpacity: 1,
                  }}
                />
              ))}
            <GeolocationControl options={{ float: 'left' }} />
          </Map>
        </div>
      </YMaps>
    </div>
  );
};

export default MyMap;
