import React, { useState } from 'react';
import { IonModal, IonButton, IonContent, IonApp, IonText, IonImg } from '@ionic/react';
import Cookies from 'universal-cookie';
import './FirstWindow.css';
import ImgBus from '../assets/bus.png';

const FirstWindow: React.FC = () => {
  const [showModal, setShowModal] = useState(true);
  const cookies = new Cookies();

  return (
    <IonModal isOpen={showModal} cssClass="my-custom-class">
      <IonApp class="first-window-class">
        <div className="icon-name-class">
          <IonImg class="icon-class" src={ImgBus} />
          <IonText class="first-name-class">Bus Board</IonText>
          <IonText class="second-name-class">Маршрутки твоего города</IonText>
        </div>
        <IonButton
          color="light"
          shape="round"
          class="close-button-first"
          onClick={() => {
            cookies.set('isNotFirstVisit', true);
            setShowModal(false);
          }}>
          <IonText class="close-btn-text">Закрыть</IonText>
        </IonButton>
      </IonApp>
    </IonModal>
  );
};

export default FirstWindow;
