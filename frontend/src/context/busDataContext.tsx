import React, { Props, useState } from 'react';
import busesJson from '../assets/busDate.json';

export interface IBusState {
  buses: IBus[];
}

export interface IBus {
  numberRoute: string;
  price: string;
  fromTo: string;
  cars: ICars[];
  pathTo: IStoppings[];
  pathFrom: IStoppings[];
}

export interface IStoppings {
  coordinates: number[];
  name?: string;
}

export interface ICars {
  numberCar: string;
  fio: string;
  phone: string;
}

const busContext = React.createContext<IBusState>({} as IBusState);

const BusProvider = (props: Props<any>) => {
  const busesState = { buses: busesJson as IBus[] } as IBusState;
  return <busContext.Provider value={busesState} {...props} />;
};

const useBusProvider = () => React.useContext(busContext);

export { BusProvider, useBusProvider };
