const express = require("express");
const mongoose = require("mongoose");
const app = express();
const bodyParser = require("body-parser");
const userRouter = require("./routes/userRouter.js");
const homeRouter = require("./routes/homeRouter.js");
 
app.set("view engine", "hbs");
app.use(bodyParser.urlencoded({ extended: false }));
 
app.use("/users", userRouter);;
app.use("/", homeRouter);
 
app.use(function (req, res, next) {
    res.status(404).send("Not Found")
});
 
mongoose.connect("mongodb://localhost:27017/usersdb", { useUnifiedTopology: true }, function(err){
    if(err) return console.log(err);
    app.listen(3000, function(){
        console.log("Сервер ожидает подключения...");
    });
});

//раскоментишь чтобы добавлять первые данные в бд
// const MongoClient = require("mongodb").MongoClient;

// const url = "mongodb://localhost:27017/";
// const mongoClient = new MongoClient(url, { useUnifiedTopology: true });

// let users = [
//   { name: "Bob", age: 34 },
//   { name: "Alice", age: 21 },
//   { name: "Tom", age: 45 },
// ];

// mongoClient.connect(function (err, client) {
//   const db = client.db("usersdb");
//   const collection = db.collection("users");

//   collection.insertMany(users, function (err, results) {
//     console.log(results);
//     client.close();
//   });
// });
